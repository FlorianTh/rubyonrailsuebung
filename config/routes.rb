Rails.application.routes.draw do
  get 'goodbye/index'

  get 'routes/index'

  get 'posts/index' #=> 'posts#index' -> diese Schreibweise für Redirect, ansosnten immer /
  get 'application/index'
  get 'posts/contact'
  root 'posts#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end


#hier stehen die Routes drin, die unsere Anwendung unterstützt
# # d.h. bevor die Anfrage zum Controller durchgeleitet wird, wird hier geguckt, ob die Route überhaupt vorhanden ist