#Florian Thom s0558101
## RubyOnRailsUebung-Project written in Ruby with Rails-Framework

* Here you can find a new Webapplication which displays Hello World -> in English and Anhamrisch
* Basically you have 2 sites: 
* -> StatUp-Page @ /
* -> Goodbye-Page @ /goodbye

##Getting Started


###Main-Versions

* Ruby version: 2.4.1

* Rails version: 5.1.6 

* gem version: 2.6.11

### Download

* change in your project-workspace and do
* $git clone https://bitbucket.org/FlorianTh/rubyonrailsuebung.git
* switch into the directory

###Installing
* Download gems: $gem install rails -v 5.1.4 --no-rdoc --no-ri
  
* Install with Bundle: bundle install
  
* Secondly start the server with: $bin/rails server
  
* You can now access http://localhost:3000 or http://localhost:3000/goodbye/index.html


##Whole dependecies (are normally auto-installed via bundle install)
actioncable (5.1.6, 5.1.4)<br>
actionmailer (5.1.6, 5.1.4)<br>
actionpack (5.1.6, 5.1.4)<br>
actionview (5.1.6, 5.1.4)<br>
activejob (5.1.6, 5.1.4)<br>
activemodel (5.1.6, 5.1.4)<br>
activerecord (5.1.6, 5.1.4)<br>
activesupport (5.1.6, 5.1.4)<br>
addressable (2.5.2)<br>
arel (8.0.0)<br>
bigdecimal (default: 1.3.0)<br>
bindex (0.5.0)<br>
builder (3.2.3)<br>
bundler (1.16.1)<br>
bundler-unload (1.0.2)<br>
byebug (10.0.2)<br>
capybara (2.18.0)<br>
childprocess (0.9.0)<br>
coffee-rails (4.2.2)<br>
coffee-script (2.4.1)<br>
coffee-script-source (1.12.2)<br>
concurrent-ruby (1.0.5)<br>
crass (1.0.4)<br>
did_you_mean (1.1.0)<br>
erubi (1.7.1)<br>
execjs (2.7.0)<br>
executable-hooks (1.3.2)<br>
ffi (1.9.23)<br>
gem-wrappers (1.3.2, 1.2.7)<br>
globalid (0.4.1)<br>
i18n (1.0.1, 0.9.5)<br>
io-console (default: 0.4.6)<br>
jbuilder (2.7.0)<br>
json (default: 2.0.2)<br>
listen (3.1.5)<br>
loofah (2.2.2)<br>
mail (2.7.0)<br>
method_source (0.9.0)<br>
mini_mime (1.0.0)<br>
mini_portile2 (2.3.0)<br>
minitest (5.11.3, 5.10.1)<br>
multi_json (1.13.1)<br>
net-telnet (0.1.1)<br>
nio4r (2.3.0)<br>
nokogiri (1.8.2)<br>
openssl (default: 2.0.3)<br>
power_assert (0.4.1)<br>
psych (default: 2.2.2)<br>
public_suffix (3.0.2)<br>
puma (3.11.4)<br>
rack (2.0.5)<br>
rack-test (1.0.0)<br>
rails (5.1.6, 5.1.4)<br>
rails-dom-testing (2.0.3)<br>
rails-html-sanitizer (1.0.4)<br>
railties (5.1.6, 5.1.4)<br>
rake (12.3.1, 12.0.0)<br>
rb-fsevent (0.10.3)<br>
rb-inotify (0.9.10)<br>
rdoc (default: 5.0.0)<br>
ruby_dep (1.5.0)<br>
rubygems-bundler (1.4.4)<br>
rubyzip (1.2.1)<br>
rvm (1.11.3.9)<br>
sass (3.5.6)<br>
sass-listen (4.0.0)<br>
sass-rails (5.0.7)<br>
selenium-webdriver (3.11.0)<br>
spring (2.0.2)<br>
spring-watcher-listen (2.0.1)<br>
sprockets (3.7.1)<br>
sprockets-rails (3.2.1)<br>
sqlite3 (1.3.13)<br>
test-unit (3.2.3)<br>
thor (0.20.0)<br>
thread_safe (0.3.6)<br>
tilt (2.0.8)<br>
turbolinks (5.1.1)<br>
turbolinks-source (5.1.0)<br>
tzinfo (1.2.5)<br>
uglifier (4.1.10)<br>
web-console (3.6.1)<br>
websocket-driver (0.6.5)<br>
websocket-extensions (0.1.3)<br>
xmlrpc (0.2.1)<br>
xpath (3.0.0)<br>